use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
struct Command {
    name: String,
    params: Vec<String>,
}

fn read_input_file(filename: String) -> Result<Vec<String>, Box<dyn Error>> {
    let mut file = File::open(filename)?;
    let mut buffer = String::new();

    file.read_to_string(&mut buffer)?;
    let result_lines: Vec<String> = buffer.split("\n").map(|s| s.to_string()).collect();

    Ok(result_lines)
}

fn get_commands_from_file_name<'a>(filename: String) -> Vec<Command> {
    let raw_string_list = match read_input_file(filename) {
        Ok(res) => res,
        Err(_) => panic!("Input file not found."),
    };

    let mut commands: Vec<Command> = vec![];
    for cmd in raw_string_list {
        let command_units: Vec<String> = cmd.split(" ").map(|s| s.to_string()).collect();
        if command_units[0] != "" {
            commands.push(Command {
                name: command_units[0].clone(),
                params: command_units[1..].to_vec().clone(),
            });
        }
    }

    commands
}

fn main() {
    let filename = String::from("input.txt");
    let commands = get_commands_from_file_name(filename);

    for command in commands {
        match command.name.as_str() {
            "list_available_rooms" => println!("do list_available_rooms"),
            _ => println!("Unrecognized command."),
        }
    }
}
